# Task


## Install process

```
php artisan migrate --seed
```


## Available URLS

`/projects` shows a list of projects.

`/projects/{project}/tasks` show list of tasks for project.

`/projects/{project}/tasks/create` Stores new task with random text. (avoiding form)

`/projects/{project}/tasks/{task}/finish` Finish a task

