<?php

namespace App\Modules\ProjectManagement\Controllers;

use App\Http\Controllers\Controller as BaseController;
use App\Modules\ProjectManagement\Repositories\TaskRepository;
use App\Modules\ProjectManagement\Repositories\ProjectRepository;


class ProjectsController extends BaseController {

    protected $projectRepository;

    function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * Render all Projects
     * @return View
     */
    public function index()
    {
        $projects = $this->projectRepository->all();

        return view('ProjectManagement::projects.index', compact('projects'));
    }


    public function show($projectId)
    {
        $project = $this->projectRepository->getByIdWithTasks($projectId);
        return view('ProjectManagement::tasks.index', compact('project'));
    }

}
