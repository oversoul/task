<?php

namespace App\Modules\ProjectManagement\Controllers;

use App\User;
use Faker\Generator as Faker;
use App\Http\Controllers\Controller as BaseController;
use App\Modules\ProjectManagement\Repositories\TaskRepository;
use App\Modules\ProjectManagement\Repositories\ProjectRepository;


class TasksController extends BaseController {

    protected $taskRepository;
    protected $projectRepository;

    function __construct(TaskRepository $taskRepository, ProjectRepository $projectRepository)
    {
        $this->taskRepository = $taskRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * Render all tasks
     * @return View
     */
    public function index()
    {
        $tasks = $this->taskRepository->all();

        return view('ProjectManagement::tasks.index', compact('tasks'));
    }


    public function store($projectId, Faker $faker)
    {
        // validate inputs | using FormRequest

        // user to which task is assigned.
        $user = User::inRandomOrder()->first();

        // faking user auth
        $auth = User::inRandomOrder()->first();

        // get project
        $project = $this->projectRepository->getById($projectId);

        // creating task
        $task = $this->taskRepository->create(
            $user,
            $faker->paragraph,
            now()->addDays(10),
            $auth
        );

        // assign task to project
        $this->projectRepository->assignTask($project, $task);


        return back();
    }


    public function finish($projectId, $taskId)
    {
        $task = $this->taskRepository->getByProjectIdAndTaskId($projectId, $taskId);

        $this->taskRepository->complete($task);

        return back();
    }

}
