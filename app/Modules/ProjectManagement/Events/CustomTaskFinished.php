<?php

namespace App\Modules\ProjectManagement\Events;

use Illuminate\Queue\SerializesModels;
use App\Modules\ProjectManagement\Models\Task;

class CustomTaskFinished
{
    use SerializesModels;

    public $task;

    /**
     * Create a new event instance.
     *
     * @param  task  $task
     * @return void
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }
}
