<?php

namespace App\Modules\ProjectManagement\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Modules\ProjectManagement\Events\CustomTaskFinished;

class UpdateClientInfo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PodcastWasPurchased  $event
     * @return void
     */
    public function handle(CustomTaskFinished $event)
    {
        $event->task->update([
            'comment'   =>  'updated from event',
        ]);
    }
}
