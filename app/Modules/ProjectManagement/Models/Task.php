<?php

namespace App\Modules\ProjectManagement\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    protected $casts = [
        'creator_id'    =>  'int',
        'user_id'       =>  'int',
        'project_id'    =>  'int',
        'is_finished'   =>  'bool',
        'due_date'      =>  'date',
    ];

    protected $fillable = [
        'comment', 'due_date', 'user_id', 'creator_id', 'project_id'
    ];

    /**
     * Get task creator
     * @return BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    /**
     * Get task user
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get task creator
     * @return BelongsTo
     */
    public function setComplete()
    {
        $this->is_finished = true;
        $this->save();

        // if json, make a loop
        if ( $this->completion_event ) {
            $eventClass = $this->completion_event;

            event(new $eventClass($this));
        }
    }
}
