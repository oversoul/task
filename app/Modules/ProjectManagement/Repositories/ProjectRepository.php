<?php
namespace App\Modules\ProjectManagement\Repositories;

use App\User;
use App\Modules\ProjectManagement\Models\Task;
use App\Modules\ProjectManagement\Models\Project;

class ProjectRepository {

    public function all()
    {
        return Project::all();
    }

    public function create(String $name): Project
    {
        $user_id = auth()->id();

        return Project::create(compact('name', 'user_id'));
    }

    public function getById($id)
    {
        return Project::findOrFail($id);
    }

    public function getByIdWithTasks($id)
    {
        return Project::with('tasks')->findOrFail($id);
    }

    public function assignTask(Project $project, Task $task)
    {
        $project->tasks()->save($task);
        return $this;
    }


}
