<?php
namespace App\Modules\ProjectManagement\Repositories;

use App\User;
use App\Modules\ProjectManagement\Models\Task;

// we can interface this.
class TaskRepository {

    public function all()
    {
        return Task::all();
    }

    public function create(User $user, String $comment, $due_date, ?User $creator = null): Task
    {
        $user_id = $user->id;
        $creator_id = $creator ? $creator->id : auth()->id();

        return Task::create(compact('comment', 'due_date', 'user_id', 'creator_id'));
    }

    public function getByProjectIdAndTaskId($project, $task)
    {
        return Task::where(['project_id' => $project, 'id' => $task])->firstOrFail();
    }

    public function complete(Task $task)
    {
        // delegate to model
        return $task->setComplete();
    }


}
