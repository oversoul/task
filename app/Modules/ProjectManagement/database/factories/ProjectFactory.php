<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Modules\ProjectManagement\Models\Project;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'name'     => $faker->sentence,
        'user_id'  => factory(User::class),
    ];
});
