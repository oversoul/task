<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Modules\ProjectManagement\Models\Task;
use App\Modules\ProjectManagement\Models\Project;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'comment'       => $faker->paragraph,
        'due_date'      => $faker->date,
        'user_id'       => factory(User::class),
        'creator_id'    => factory(User::class),
        'project_id'    => factory(Project::class),
    ];
});
