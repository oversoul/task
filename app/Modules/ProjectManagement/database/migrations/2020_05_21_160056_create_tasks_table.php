<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->date('due_date');
            $table->text('comment')->nullable();
            $table->integer('user_id')->default(0);
            $table->integer('creator_id')->default(0);
            $table->integer('project_id')->default(0);

            $table->boolean('is_finished')->default(false);
            $table->string('completion_event')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
