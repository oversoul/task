<?php

use Illuminate\Support\Facades\Route;

/*
All routes here are prefixed with '/projects'
*/

Route::get('/', 'ProjectsController@index')->name('projects');
Route::get('/{project}/tasks', 'ProjectsController@show')->name('tasks');

// fake form
Route::get('/{project}/tasks/create', 'TasksController@store')->name('create.task');

Route::get('/{project}/tasks/{task}/finish', 'TasksController@finish')->name('finish.task');
