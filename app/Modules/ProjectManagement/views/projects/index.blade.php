@extends('ProjectManagement::layout')

@section('content')
<div class="bg-white shadow overflow-hidden sm:rounded-lg p-4">
    <ul>
        @foreach($projects as $project)
        <li class="border-t border-gray-200 pl-3 pr-4 py-3 flex items-center justify-between text-sm leading-5">
            <div class="w-0 flex-1 flex items-center">
                <span class="ml-2 flex-1 w-0 truncate">
                    <a href="{{ route('project-management.tasks', $project) }}">{{ $project->name }}</a>
                </span>
            </div>
        </li>
        @endforeach
    </ul>


    {{-- this actually can be done inside a blade directive --}}
    @foreach(event('modules.project-management.project.index') as $evt)
        {{ $evt }}
    @endforeach
</div>
@endsection
