@extends('ProjectManagement::layout')

@section('content')

<div class="bg-white shadow overflow-hidden sm:rounded-lg">
    <div class="px-4 py-5 border-b border-gray-200 sm:px-6 flex justify-between">
        <h3 class="text-lg leading-6 font-medium text-gray-900">
            {{ $project->name }}
        </h3>

        <div>
            <a href="{{ route('project-management.create.task', $project) }}" class="px-5 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline transition duration-150 ease-in-out">Create Random Task</a>

            <a href="{{ route('project-management.projects') }}" class="px-5 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-gray-700 hover:bg-gray-500 hover:text-white focus:outline-none focus:shadow-outline transition duration-150 ease-in-out">Back</a>
        </div>
    </div>

    @forelse($project->tasks as $task)
    <div class="border-b">
        <dl>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm leading-5 font-medium text-gray-500">
                    Comment
                </dt>
                <dd class="mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2">
                    {{ $task->comment }}
                </dd>
            </div>
        </dl>
        <dl>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm leading-5 font-medium text-gray-500">
                    Due Date
                </dt>
                <dd class="mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2">
                    {{ $task->due_date->format('Y-m-d') }}
                </dd>
            </div>
        </dl>
        <dl>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm leading-5 font-medium text-gray-500">
                    Actions
                </dt>
                <dd class="mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2">
                    @if ($task->is_finished)
                        <span class="inline-block rounded-lg font-medium leading-none py-2 px-3 focus:outline-none bg-indigo-50 text-indigo-700">Finished</span>
                    @else
                        <a href="{{ route('project-management.finish.task', compact('project', 'task')) }}" class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline transition duration-150 ease-in-out">Finish</a>
                    @endif
                </dd>
            </div>
        </dl>
    </div>
    @empty

    <div class="border-b">
        <dl>
            <div class="bg-gray-50 px-4 py-5 sm:px-6">
                <dt class="text-sm leading-5 font-medium text-gray-500 text-center py-10">
                    No tasks.
                </dt>
            </div>
        </dl>
    </div>

    @endforelse
</div>


@endsection
