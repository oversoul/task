<?php

namespace App\Modules\Tenants\Services;

use App\Providers\AppServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Event;
use App\Modules\Tenants\Widgets\ShowClientsList;


class TenantsServiceProvider extends AppServiceProvider {

    /**
     * Controller namespaces
     *
     * @var string
     */
    protected $namespace = 'App\Modules\Tenants\Controllers';

    protected $listen = [
        'modules.project-management.project.index' => ShowClientsList::class,
    ];


    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // $this->mapWebRoutes();

        // $this->loadDatabaseConfig();

        $this->registerEvents();
    }

    /**
     * Register global module events
     * @return void
     */
    protected function registerEvents(): void
    {
        foreach ($this->listen as $event => $listener) {
            Event::listen($event, $listener);
        }
    }

    /**
     * Add database folder to laravel core.
     * @return void
     */
    protected function loadDatabaseConfig(): void
    {
        $this->loadMigrationsFrom(
            $this->getModulePath('database/migrations')
        );

        $this->loadFactoriesFrom(
            $this->getModulePath('database/factories')
        );
    }

    /**
     * Define the "web" routes for the module.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            // make all routes prefixed with /projects
            ->prefix('projects')
            // make all routes names prefixed with project-management.
            ->as('project-management.')
            // controllers namespace
            ->namespace($this->namespace)
            // register module routes
            ->group($this->getModulePath('routes/web.php'));
    }

    /**
     * Get module's current folder
     * @param  string $path
     * @return string
     */
    protected function getModulePath($path = ''): string
    {
        return app_path("Modules/Tenants/{$path}");
    }


    /**
     * Add namespace to view
     * view('Tenants::tasks.index')
     *     => modulefolder/views/tasks/index.blade.php
     *
     * @return void
     */
    public function register()
    {
        view()->addNamespace('Tenants', $this->getModulePath('views'));
    }

}
