<?php

namespace App\Modules\Tenants\Widgets;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShowClientsList
{
    protected $jobService;

    /**
     * use laravel container to get job service instance
     * JobService $jobService
     */
    public function __construct()
    {
        // setting it to dummy data.
        $this->jobService = [
            'title'     =>  'Some Job title',
            'subtitle'  =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius blanditiis qui molestiae, illum animi incidunt, eveniet hic, nisi laudantium corrupti accusamus autem voluptatum ipsa quibusdam quis neque natus, ad beatae.'
        ];
    }


    public function handle()
    {
        return view('Tenants::clients.index')
            ->withJob($this->jobService);
    }
}
