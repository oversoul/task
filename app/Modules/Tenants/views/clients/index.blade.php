{{-- the widget views should not have --}}

<div class="p-5">

    <h2 class="text-xl">This is rendered from `Modules/Tenants/views/clients/index.balde.php`</h2>

    <div class="rounded border p-2 bg-gray-700 text-white mt-2">the data is rendered from `App\Modules\Tenants\Widgets\ShowClientsList.php`</div>

    <div class="mt-3">
        <h4 class="text-lg">Job info</h4>
        <hr>

        <div class="py-5 flex">
            <div class="font-bold w-1/4">Job Title</div>

            <div class="w-3/4">{{ $job['title'] }}</div>
        </div>

        <div class="py-5 flex justify-between">
            <div class="font-bold w-1/4">Job Subtitle</div>

            <div class="w-3/4">{{ $job['subtitle'] }}</div>
        </div>

    </div>
</div>
