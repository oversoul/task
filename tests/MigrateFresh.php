<?php

namespace Tests;

use Illuminate\Support\Facades\Artisan;

trait MigrateFresh
{

    /**
     * After the first run of setUp "migrate:fresh --seed"
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate:fresh');
    }
}
