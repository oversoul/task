<?php

namespace Tests\Unit;

use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use App\Modules\ProjectManagement\Models\Task;
use App\Modules\ProjectManagement\Repositories\TaskRepository;

class TasksTest extends TestCase
{

    protected $taskRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->taskRepository = new TaskRepository();
    }

    /** @test */
    public function canGetListOfTasks()
    {
        factory(Task::class, 5)->create();

        // sanity check
        $this->assertEquals(Task::count(), 5);

        $tasks = $this->taskRepository->all();

        $randomTask = $tasks->random();

        $this->assertDatabaseHas('tasks', [
            'user_id'    => $randomTask->user_id,
            'comment'    => $randomTask->comment,
            'due_date'   => $randomTask->due_date,
            'creator_id' => $randomTask->creator_id,
        ]);
    }

    /** @test */
    public function canCreateTask()
    {
        $authUser = factory(User::class)->create();

        $this->actingAs($authUser);

        $userToAssign = factory(User::class)->create();

        $newTask = $this->taskRepository->create($userToAssign, 'random comment', Carbon::today());

        $this->assertDatabaseHas('tasks', [
            'user_id'    => $newTask->user_id,
            'creator_id' => $newTask->creator_id,
            'due_date'   => $newTask->due_date,
            'comment'    => $newTask->comment,
        ]);
    }

    /** @test */
    public function canFinishTask()
    {
        $task = factory(Task::class)->create();

        $this->taskRepository->complete($task);

        $this->assertTrue(Task::first()->is_finished);
    }

    /** @test */
    public function canTriggerEventAfterTaskIsFinished()
    {
        $task = factory(Task::class)->create();

        $task->completion_event = \App\Modules\ProjectManagement\Events\CustomTaskFinished::class;

        $task->save();

        $this->taskRepository->complete($task);

        $completedTask = Task::first();

        $this->assertTrue($completedTask->is_finished);

        // the comment is set in the event handler.
        $this->assertEquals($completedTask->comment, 'updated from event');
    }
}
